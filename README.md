# Slugify All

## Description
A Python script to "slugify" all folders and file names. Really useful when you need to normalize your public folder.

## Installation
Install [python-slugify](https://github.com/un33k/python-slugify)
```
pip3 install python-slugify
```

## Usage
- Copy and paste `slugify-all.py` inside the folder.
- Run it: `python3 slugify-all.py`.

## Support
Please contact me via [Telegram](https://t.me/x4fingers) if you have any issues.

## License
This project is licensed in GPL v3.

## Project status
Finished and waiting for issues.

