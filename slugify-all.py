import os
from pathlib import Path
# Slugify: https://github.com/un33k/python-slugify
from slugify import slugify

# Defines excluded files
excluded_files = ['slugify-all.py', '.DS_Store']

# Modify your directory path here or use the default
directory = os.getcwd()

roots = []

# Step 1: Rename files
for subdir, dirs, files in os.walk(directory):
    for file in files:
        file_stem = Path(file).stem
        file_suffix = Path(file).suffix

        if file_stem == slugify(file_stem):
            print("Skipped file name:", file)

        if file not in excluded_files and file_stem != slugify(file_stem):
            subdirectoryPath = os.path.relpath(subdir, directory)
            file_path = os.path.join(subdirectoryPath, file)
            new_file = slugify(file_stem) + file_suffix
            new_file_path = os.path.join(subdirectoryPath, new_file)

            print("Old file name:", file_path)
            print("New file name:", new_file_path)

            os.rename(file_path, new_file_path)

# Step 2: Rename folders from bottom up
for root, dirs, files in os.walk(directory, topdown=False):
    # Prevent add current directory path to roots array
    if root != directory:
        roots.append(root)

    print('**********')
    print("Now rename child folders in:", root, "\n")

    for dir in dirs:
        if (os.path.join(root, dir) != os.path.join(root, slugify(dir))):
            dir_path = os.path.join(root, dir)
            new_dir_path = os.path.join(root, slugify(dir))

            print('----------')

            try:
                os.rename(dir_path, new_dir_path)
                print("Old folder name:", dir_path)
                print("New folder name:", new_dir_path)
            except OSError as e:
                if e.errno == 66:
                    print('Skip renamed folder:', new_dir_path)
                

# Step 3: Rename folders are child of directory path, a.k.a root in above loop of os.walk()
for dir_path in roots:
    dir_path_splited = dir_path.split('/')
    dir_name_index = len(dir_path_splited) - 1
    dir_name = dir_path_splited[dir_name_index]

    if dir_name != slugify(dir_name):
        dir_path_splited[dir_name_index] = slugify(dir_name)
        new_dir_name = dir_path_splited[dir_name_index]
        new_dir_path = "/".join(dir_path_splited)

        print('----------')

        try:
            os.rename(dir_path, new_dir_path)
            print("Old folder name:", dir_path)
            print("New folder name:", new_dir_path)
        # Skip and notice if exist folder
        except OSError as e:
            if e.errno == 66:
                print('Skip renamed folder:', new_dir_path)

print("\n**********\nRename finished.\n**********")
